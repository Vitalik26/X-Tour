<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
//define('DB_NAME', '777-dev_xtur_db');
define('DB_NAME', 'xTour');


/** Имя пользователя MySQL */
//define('DB_USER', '777-dev_xtur_usr');
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
//define('DB_PASSWORD', 'q0fNMGTrnr');
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
//define('DB_HOST', '144.76.230.227');
//define('DB_HOST', 'localhost');
define('DB_HOST', '192.168.0.107');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`-NVK/A<47A_L<_V!gQyd4GEz/-OKX#b%f-qB7F+}NzV/N*J$Ax*@X:|jTfik!IG');
define('SECURE_AUTH_KEY',  'dKg;Qv47@m+m.$e82yC)V&vrv|/Me|0&4SyR/nGz>c9%^+HaV|ZlnV.P=`3Qs3p|');
define('LOGGED_IN_KEY',    'il=!;0 Esl]wu&IpE3K|ur$~S528CttddRc1{hr[fa#nEOKD]$]x0v-~I@0o{L4V');
define('NONCE_KEY',        'u| ^fxc[SP[*$,jZ#aYU&6[pUO|QReRllMl=qo[Y*C16XIdRx4HcAL*<e.ZFmlRC');
define('AUTH_SALT',        '{qAj28Jfj<rC5Mb}MKwU6B=J7iytL;E1-T;l!-hHyRo`.&40L:4Tq2/1UOtIsw#~');
define('SECURE_AUTH_SALT', 'YD%u:1O}CA,>G7Sisf`pYV1,qtp~J&+6{>5y0C~3J:HC)[hp|NQ<+-rdDX&U$[?X');
define('LOGGED_IN_SALT',   ' <O 8f)NeS)R(hR8bsX>E8.FnjokvSd0<=/LA`vwkdC9g#q;I]llIb3x?6OjtU:B');
define('NONCE_SALT',       'H=K9ykRC>J4AN4d)u0Y_ADXAPN0>E}E0_C-G4t^EV8|Bh#+s,Qj_UZ>y}Kq9r4[6');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'ct_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
