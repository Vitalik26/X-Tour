<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();?>

            <div id="slider-section">
<div id="slider">
    <div><img style="width: 100%" class="slider_1 "  src="<?php echo get_the_post_thumbnail_url() ?>" />
      <div class="content_slider elem" data-speed="-25">
          <span class="">ПОХОД</span>
<!--          <h2 class="">Неистовая<br>Говерла</h2>-->
          <h2 class="" ><?php the_title() ?></h2>
          <span class="bold" >3 дня | <span style="color: #00a1ec">22.07. - 24.07</span></span>

      </div>
  </div>
</div>
</div>

<style>
   #direct-section2 .direct_bl {
    padding-bottom: 115px;
}
</style>

<?php
get_template_part( 'template-parts/page/content', 'page' );
get_template_part( 'template-parts/navigation/navigation', 'top' );
?>

</div>

<div id="dates-section" >

    <h2 class="elem" data-speed="5"><?php echo get_field('Title_coming_dates') ?></h2>
     <div class="container elem" data-speed="5">
            <div class="row" >
                <div class="col-md-3 date_block" >
                    <a href="#"><img src="/wp-content/themes/default/assets/images/arrow_up.png" />Предыдущий</a>
                    <div class="date_tur"><span>Дата тура</span><h3 class="black">20.09-10.10</h3></div>
                    <a href="#">Следующий<img src="/wp-content/themes/default/assets/images/arrow_down.png" /></a>
                    <div class="calendar_icon"><img src="/wp-content/themes/default/assets/images/date-cal_icon.png" /></div>
                </div>
                <div class="col-md-9 event_bl" >
                    <div class="event_info">

                        <div class="free_m">Общее количество мест: <span>25</span></div>
                        <h2 class="black"><?php the_title() ?></h2>
                        <p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне бла бла. </p>
                        <div class="col-md-6 price black">1888 грн</div> <div class="col-md-6 book"><a href="#" class="b">Забронировать место<img src="/wp-content/themes/default/assets/images/next_1.png" /></a></div>
                    </div>

                </div>
            </div>
    </div>
    <div id="call-section" >
       <div class="row" >
         <h2><?php echo get_field('call_order_header') ?></h2>
         <p><?php echo get_field('call_order_text') ?></p>
           <?php echo do_shortcode('[contact-form-7 id="85" title="Contact form 1"]') ?>

       </div>

    </div>

</div>

<div id="programm-tur-section" >
    <h2><?php echo get_field('title_the_tour_program') ?></h2>
     <div class="container">
            <div class="row" >
                <p><?php echo get_field('text_the_tour_program') ?></p>
                <div><a href="#"><img src="/wp-content/themes/default/assets/images/arr_down_w.png" /></a></div>
            </div>
    </div>
</div>
<div id="programm-tur-section-day-1" >
 <h2><span>1</span>день</h2>
     <div class="container">
            <div class="" id="slider_event">
                <div class="cont_event col-md-6"><div class="event"><a href="#"><div class="img_content"><div class="city b">7:00</div><img src="/wp-content/themes/default/assets/images/press_1.jpg" /></div><p class="black">Почему он используется</p><span>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</span></a></div></div>
                <div class="cont_event col-md-6"><div class="event"><a href="#"><div class="img_content"><div class="city b">7:00</div><img src="/wp-content/themes/default/assets/images/press_1.jpg" /></div><p class="black">Почему он используется</p><span>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</span></a></div></div>
                <div class="cont_event col-md-6"><div class="event"><a href="#"><div class="img_content"><div class="city b">7:00</div><img src="/wp-content/themes/default/assets/images/press_1.jpg" /></div><p class="black">Почему он используется</p><span>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</span></a></div></div>
                <div class="cont_event col-md-6"><div class="event"><a href="#"><div class="img_content"><div class="city b">7:00</div><img src="/wp-content/themes/default/assets/images/press_1.jpg" /></div><p class="black">Почему он используется</p><span>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</span></a></div></div>

            </div>
    </div>
</div>
<div id="programm-tur-section-day-1" class="programm-tur-section-day-2" >
 <h2><span>2</span>день</h2>
     <div class="container">
            <div class="" id="slider_event">
                <div class="cont_event col-md-6"><div class="event"><a href="#"><div class="img_content"><div class="city b">7:00</div><img src="/wp-content/themes/default/assets/images/press_1.jpg" /></div><p class="black">Почему он используется</p><span>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</span></a></div></div>
                <div class="cont_event col-md-6"><div class="event"><a href="#"><div class="img_content"><div class="city b">7:00</div><img src="/wp-content/themes/default/assets/images/press_1.jpg" /></div><p class="black">Почему он используется</p><span>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</span></a></div></div>

            </div>
    </div>
</div>
<div id="slider-photos">
<div class="owl-carousel ">
    <div class="item"><img src="/wp-content/themes/default/assets/images/slide_2.jpg" /></div>
     <div class="item"><img src="/wp-content/themes/default/assets/images/slide_2.jpg" /></div>

</div>
   <script>
    jQuery(document).ready(function(){
    jQuery("#slider-photos .owl-carousel").owlCarousel({
    loop:true,
 items:1,
 nav: true,
     navText: ['<img src="/wp-content/themes/default/assets/images/prev_2.png" />','<img src="/wp-content/themes/default/assets/images/next_2.png" />']
   });});</script>
</div>
<?php
get_template_part( 'template-parts/page/content', 'front-page' );
?>
<div id="ready-section">

     <div class="container">
            <div class="row read" >
                <div class="col-md-5">
                     <h2 >Решаешься?</h2>
                     <a href=""><img src="/wp-content/themes/default/assets/images/ready_button.png" /></a>

                </div>
                <div class="col-md-7">
                    <p>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта.</p>
                </div>
            </div>
         <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
 <div id="dates-section" >

    <h2>Ближайшие даты</h2>
     <div class="container">
            <div class="row" >
                <div class="col-md-3 date_block" >
                    <a href="#"><img src="/wp-content/themes/default/assets/images/arrow_up.png" />Предыдущий</a>
                    <div class="date_tur"><span>Дата тура</span><h3 class="black">20.09-10.10</h3></div>
                    <a href="#">Следующий<img src="/wp-content/themes/default/assets/images/arrow_down.png" /></a>
                    <div class="calendar_icon"><img src="/wp-content/themes/default/assets/images/date-cal_icon.png" /></div>
                </div>
                <div class="col-md-9 event_bl" >
                    <div class="event_info">
                        <div class="free_m">Свободно мест: <span>25</span></div>
                        <h2 class="black">Неистовая Говерла</h2>
                        <p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне бла бла. </p>
                        <div class="col-md-6 price black">1888 грн</div> <div class="col-md-6 book"><a href="#" class="b">Забронировать место<img src="/wp-content/themes/default/assets/images/next_1.png" /></a></div>
                    </div>

                </div>
            </div>
    </div>
</div>

    </div>
</div>

<?php
get_template_part( 'template-parts/page/content', 'front-page-panels' );
?>


<?php

				get_template_part( 'template-parts/post/content', get_post_format() );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

				the_post_navigation( array(
					'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'twentyseventeen' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '</span>%title</span>',
					'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'twentyseventeen' ) . '</span> <span class="nav-title">%title<span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ) . '</span></span>',
				) );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->



<?php get_footer();
