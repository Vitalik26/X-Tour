<?php
 /*
    Template Name: Contact
    */

get_header(); ?>


<div id="contact-main">
        <div class="see_contact"><img src="/wp-content/themes/default/assets/images/arr_down.png" /></div>
    <div class="contact-section">
  
     <div class="container">
         <div class="row">
        

                <h2>Контакты</h2>
                <div class="col-md-6 text-left cont_bl" style="">
                  <h3 >Адрес</h3><p>Киев, ул. Руденко 21А (7 мин. пешком от м. Позняки)</p>
                 <span>Перед визитом, пожалуйста, обязательно звоните — 
                мы можем быть на рафтинге или в походе.</span>
                  <h3>Телефон</h3><p>(073) 115-31-13  |  (066) 215-31-13</p>
                   <h3>E-mail:</h3><p>info@foxtravel.com.ua</p>
                    <div class="social">
                       <a href="#" class=""><img src="/wp-content/themes/default/assets/images/cont_icon_1.png"/></a>
                       <a href="#" class=""><img src="/wp-content/themes/default/assets/images/cont_icon_2.png"/></a>
                       <a href="#" class=""><img src="/wp-content/themes/default/assets/images/cont_icon_3.png"/></a>
                       <a href="#" class=""><img src="/wp-content/themes/default/assets/images/cont_icon_4.png"/></a>
                    </div>
                  
                  
                </div>
                <div class="col-md-6  text-left">
                    <div class="form_cont">
                    <h3 style="    text-transform: uppercase;">Есть вопросы<br>или предложения?</h3>
                    <input type="name" class="name" placeholder="Имя" />
                    <input type="email" class="email" placeholder="E-mail" />
                    <textarea name="messege" placeholder="Сообщение"></textarea>
                    <input type="submit" class="submit black" value="Отправить" />
                </div> </div>    
                <div class="see_cart black">Посмотреть на карте<br><img src="/wp-content/themes/default/assets/images/arr_down.png" /></div>
                
                
         </div> 

        </div>
    </div>
    
    
    
 <div id="map"></div>

    <script>
  
        
     function initMap() {  
    var coordinates = {lat: 50.393537, lng: 30.643520},
    
        map = new google.maps.Map(document.getElementById('map'), {
            center: coordinates,
    zoom: 17
        });
        
var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
       '<div id="bodyContent">'+
      '<p>вулиця Лариси Руденко, 21' +
 
      '</div>'+
      '</div>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  var marker = new google.maps.Marker({
    position: coordinates,
    map: map,
    title: 'Uluru (Ayers Rock)'
  });
     infowindow.open(map, marker);
 
      }
 
 
</script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2vezmE5LqnSwzsZmfgoc4p3Z85xdfQZg&callback=initMap" async defer></script>

</div>

<?php get_footer();
