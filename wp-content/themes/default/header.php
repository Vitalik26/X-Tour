<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
 <?php wp_head(); ?>
<link href="/wp-content/themes/default/assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="/wp-content/themes/default/assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="/wp-content/themes/default/assets/css/owl.theme.default.min.css">
<link rel="stylesheet" href="/wp-content/themes/default/assets/css/animate.min.css">
<script src="/wp-content/themes/default/assets/js/wow.min.js" ></script>
<script>
  jQuery(function($){
   $(".telephone").mask("(999) 999-9999");
   });
new WOW().init();
</script>
</head>
<body>
    <div id="menu-header" class="navbar-fixed-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-7 left_menu">
                        <div class="logo">

                            <?php
                            $image = get_field('logo_img' , 'option');
                            if( !empty($image) ): ?>
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                            <?php endif; ?>

                        </div>
                        <div class="phone"><a href="tel:<?php echo get_field('phone_part1red' , 'option') ?><?php echo get_field('phone_part2white' , 'option') ?>""><span style="color: #ed3f1c"><?php echo get_field('phone_part1red' , 'option') ?></span><?php echo get_field('phone_part2white' , 'option') ?></a></div>
                        <div class="social">

                            <?php if(get_field('social' , 'option')): ?>
                                    <?php while(has_sub_field('social' , 'option')): ?>
                                <?php $image = get_sub_field('image', 'option');
                                    $link = get_sub_field('link' , 'option');
                                    ?>
                                    <a href="<?php echo $link; ?>" class=""><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" /></a>
                                    <?php endwhile; ?>
                            <?php endif; ?>

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-5 text-right">
                      <div class="row">
                        <ul class="top-right-menu">
                            <?php wp_nav_menu( array( 'default' => 'footer_header_menu' ) ); ?>

                        </ul>
                          <ul style="padding-left: 10px;" class="top-right-menu ">
<!--                            <li><a href="#">EN</a></li><b style="color: #fff;    font-size: 11px;">/</b>-->
<!--                            <li><a href="#">RU</a></li>-->
                              <li><a href="#">  </a></li><b style="color: #fff;    font-size: 11px;"></b>
                              <li><a href="#">  </a></li>

                        </ul>
                      </div>
                     <div class="row top-right-bottom hidden-xs">
                         <?php if(get_field('custom_menu_under_the_main_menu' , 'option')): ?>
                             <?php while(has_sub_field('custom_menu_under_the_main_menu' , 'option')): ?>
                                 <?php $image = get_sub_field('img', 'option');
                                 $title_page = get_sub_field('title_page' , 'option');
                                 $link = get_sub_field('link_to_page' , 'option');
                                 ?>
                                 <div style="margin-right: 25px;"><a href="<?php echo $link; ?>"><img style="height: 17px;"  src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>"/><?php echo  $title_page; ?></a></div>
                             <?php endwhile; ?>
                         <?php endif; ?>
                     </div>


                    </div>

                    <div class="col-md-2 col-sm-12 hidden-xs afterp" >
                         <a class="top-right-button">AFTERPARTY</a>

                    </div>


                </div>

             </div>

    </div>

    <div id="main">