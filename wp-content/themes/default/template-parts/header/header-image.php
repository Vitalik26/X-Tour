<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div id="dates-section" >
    
    <h2>Ближайшие даты</h2>
     <div class="container">
            <div class="row" >
                <div class="col-md-3 date_block" >
                    <a href="#"><img src="/wp-content/themes/default/assets/images/arrow_up.png" />Предыдущий</a>
                    <div class="date_tur"><span>Дата тура</span><h3 class="black">20.09-10.10</h3></div>
                    <a href="#">Следующий<img src="/wp-content/themes/default/assets/images/arrow_down.png" /></a>
                    <div class="calendar_icon"><img src="/wp-content/themes/default/assets/images/date-cal_icon.png" /></div>
                </div>
                <div class="col-md-9 event_bl" >
                    <div class="event_info">
                        <div class="free_m">Свободно мест: <span>25</span></div>
                        <h2 class="black">Неистовая Говерла</h2>
                        <p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне бла бла. </p>
                        <div class="col-md-6 price black">1888 грн</div> <div class="col-md-6 book"><a href="#" class="b">Забронировать место<img src="/wp-content/themes/default/assets/images/next_1.png" /></a></div>
                    </div>
                    
                </div>
            </div>
    </div>
