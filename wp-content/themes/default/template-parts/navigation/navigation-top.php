<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<div id="include-section" class="elem" data-speed="5">
  
     <div class="container">
            <div class="row" >
               <div class="tabs">
                    <ul>
                        <li class="active"  data-tab="tab-1"><img src="/wp-content/themes/default/assets/images/check.png" /><?php echo get_field('title_on')  ?></li>
                        <li  data-tab="tab-2"><img src="/wp-content/themes/default/assets/images/no.png" /> <?php echo get_field('title_off')  ?></li>
                     </ul>
                    <div id="tabs_p">
                        <div id="tab-1" class="tab-content current">

                            <?php if (get_field('whats_Included')): ?>
                                <?php while (has_sub_field('whats_Included')): ?>
                                    <?php
                                    $img = get_sub_field('ico');
                                    $text = get_sub_field('text');

                                    echo $date;
                                    ?>
                                    <div class="col-md-4">
                                        <p><img src="<?php echo $img['url']; ?>"
                                                alt="<?php echo $img['alt'] ?>"/><?php echo $text ?></p>
                                    </div>

                                    <?php $counter++; endwhile; ?>
                            <?php endif; ?>
                        </div>
                        <div id="tab-2" class="tab-content">
                            <?php if (get_field('not_included')): ?>
                                <?php while (has_sub_field('not_included')): ?>
                                    <?php
                                    $img = get_sub_field('ico');
                                    $text = get_sub_field('text');
                                    echo $date;
                                    ?>
                                    <div class="col-md-4">
                                        <p><img src="<?php echo $img['url']; ?>"
                                                alt="<?php echo $img['alt'] ?>"/><?php echo $text ?></p>
                                    </div>
                                    <?php $counter++; endwhile; ?>
                            <?php endif; ?>
                        </div>
                     </div>
                </div> 
            </div>
    </div>
</div>