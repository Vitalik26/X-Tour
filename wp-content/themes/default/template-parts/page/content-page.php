<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<div id="direct-section2" >
   <h2 class="wow fadeInDown"  data-wow-delay="0.3s"><?php echo get_field('title_benefits') ?></h2>
    <div class="container direct_bl">
            <div class="row" >
                <?php  $counter = 0; ?>
                <?php  $counterNumber = 1; ?>
                <?php if(get_field('benefitss' )): ?>
                    <?php while(has_sub_field('benefitss')): ?>
                        <?php
                        $text = get_sub_field('text', 'option');
                        ?>
                        <div data-wow-delay="0.<?php echo $counter+2?>s" class="col-md-3 wow fadeInUp"><span><?php echo $counterNumber?></span><div class="img_content"><?php echo $text ?></div></div>
                    <?php $counter++; $counterNumber++; endwhile; ?>
                <?php endif; ?>
            </div>
    </div>