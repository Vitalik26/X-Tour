<?php
/**
 * Template part for displaying pages on front page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

 ?>
 

<div id="review-section">
   <h2>Отзывы</h2>
     <img class="review_im hidden-xs hidden-sm" src="/wp-content/themes/default/assets/images/review_img.png" />
                 <img class="review_im2 hidden-xs hidden-sm" src="/wp-content/themes/default/assets/images/review_img_2.png" />

      <div class="container">
          <div class="row" >
                <div class="owl-carousel" id="slider_rev">
                <div class="cont_review">
                    <div class="img_grid col-md-3 col-sm-3 hidden-xs"><img src="/wp-content/themes/default/assets/images/review_1.png" />                       </div>

                    <div class="img_content col-md-9 col-sm-9">
                       <h3 class="black">Андрей</h3>
                       <p class="date">15.10.2017</p>
                       <p class=" ">Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв.</p>


                    </div>

                </div>
               <div class="cont_review">
                    <div class="img_grid col-md-3 col-sm-3 hidden-xs"><img src="/wp-content/themes/default/assets/images/review_1.png" />                       </div>

                    <div class="img_content col-md-9 col-sm-9">
                       <h3 class="black">Сергей</h3>
                       <p class="date">15.10.2017</p>
                       <p class=" ">Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв.</p>


                    </div>

                </div>
            </div>
         </div>
 <script>
jQuery(document).ready(function(){
  jQuery("#slider_rev").owlCarousel({
    loop:true,
    items:1,
    nav:true,
    navText: ['<img src="/wp-content/themes/default/assets/images/prev_1.png" />','<img src="/wp-content/themes/default/assets/images/next_1.png" />']
     });});</script>
    </div>
      <div class="wow fadeInUp container"  data-wow-delay="0.8s" >
          <div class="row" >
                <div class="owl-carousel" id="slider_rev2">
                <div class="cont_review2">

                    <div class="img_content2 black">
                        <iframe src="https://www.youtube.com/embed/TavRhEulwQk" frameborder="0" allowfullscreen></iframe>
                        <p class=" ">Поход по Турции «Ликийская сказка»</p>


                    </div>

                </div>
                <div class="cont_review2">

                    <div class="img_content2 black">
                        <iframe src="https://www.youtube.com/embed/TavRhEulwQk" frameborder="0" allowfullscreen></iframe>
                        <p class=" ">Поход по Турции «Ликийская сказка»</p>


                    </div>

                </div>
            </div>
         </div>
 <script>
jQuery(document).ready(function(){
  jQuery("#slider_rev2").owlCarousel({
    loop:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:2
        }
    },
    nav:true,
    navText: ['<img src="/wp-content/themes/default/assets/images/prev_1.png" />','<img src="/wp-content/themes/default/assets/images/next_1.png" />']
     });});</script>
    </div>
</div>
