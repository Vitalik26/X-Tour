<?php
/**
 * Displays content for front page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!-- <div id="kit-section">-->
<!--   <h2>Экипировка</h2>-->
<!--   <p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает<br>сосредоточиться. Lorem Ipsum используют потому</p>-->
<!--     <div class="container">-->
<!--          <div class="row" >-->
<!---->
<!--             <div class="owl-carousel" id="slider_kit">-->
<!--                <div class="cont_kit"><a href="#"><img src="/wp-content/themes/default/assets/images/kit_1.jpg" /><div class="img_content"><p class="black">Супер  туристический рюкзак</p><span class="price">400 грн</span><button class="btn">забронировать</button></div></a></div>-->
<!--                <div class="cont_kit"><a href="#"><img src="/wp-content/themes/default/assets/images/kit_1.jpg" /><div class="img_content"><p class="black">Супер  туристический рюкзак</p><span class="price">400 грн</span><button class="btn">забронировать</button></div></a></div>-->
<!--                <div class="cont_kit"><a href="#"><img src="/wp-content/themes/default/assets/images/kit_1.jpg" /><div class="img_content"><p class="black">Супер  туристический рюкзак</p><span class="price">400 грн</span><button class="btn">забронировать</button></div></a></div>-->
<!---->
<!--            </div>-->
<!--         </div>-->
 <script>
jQuery(document).ready(function(){
  jQuery("#slider_kit").owlCarousel({
    loop:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    },
    nav:true,
    navText: ['<img src="/wp-content/themes/default/assets/images/prev_1.png" />','<img src="/wp-content/themes/default/assets/images/next_1.png" />']
     });});</script>
<!--    </div>-->
<!--</div>-->