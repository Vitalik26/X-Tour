
jQuery(document).ready(function(){

     //contact form send hidden field url page
    var loc = window.location.href;
      jQuery('.hidden_url').attr("value" , loc )


    jQuery('#direct-section2 .tabs ul li').click(function(){
        var tab_id = jQuery(this).attr('data-tab');

        jQuery('#direct-section2 .tabs ul li').removeClass('active');
        jQuery('#direct-section2 .tab-content').removeClass('current');

        jQuery(this).addClass('active');
        jQuery("#direct-section2 #"+tab_id).addClass('current');
    })


    jQuery('#ready-section .tabs ul li').click(function(){
        var tab_id = $(this).attr('data-tab');

        jQuery('#ready-section .tabs ul li').removeClass('active');
        jQuery('#ready-section .tab-content').removeClass('current');

        jQuery(this).addClass('active');
        jQuery("#ready-section #"+tab_id).addClass('current');
    })


    jQuery(function(){

        // jQuery('#main').css('padding-top', jQuery('#menu-header').height())
        if(jQuery(window).height()<jQuery('.contact-section .container').height()) {

            jQuery('#map').height( jQuery('.contact-section .container').height()*1.1) ;

        } else {
            jQuery('#map').height( jQuery(window).height()*1.1 );

        }

        jQuery(window).resize(function() {
            // jQuery('#main').css('padding-top', jQuery('#menu-header').height())
            if(jQuery(window).height()<jQuery('.contact-section .container').height()) {

                jQuery('#map').height( jQuery('.contact-section .container').height()*1.1) ;

            } else {
                jQuery('#map').height( jQuery(window).height()*1.1 );

            }
        })
    })
    jQuery('.hide').hide();

    jQuery('.see_cart').on('click', function() {

        jQuery('.contact-section').slideUp(500);
        jQuery('.see_contact').slideDown(600);


    });

    jQuery('.see_contact').on('click', function() {
        jQuery('.see_contact').hide();
        jQuery('.contact-section').slideDown(500);


    });

});

(function(jQuery){
    jQuery.fn.lightTabs = function(options){

        var createTabs = function(){
            tabs = this;
            i = 0;

            showPage = function(i){
                jQuery(tabs).children("div").children("div").hide();
                jQuery(tabs).children("div").children("div").eq(i).show();
                jQuery(tabs).children("ul").children("li").removeClass("active");
                jQuery(tabs).children("ul").children("li").eq(i).addClass("active");
            }

            showPage(0);

            jQuery(tabs).children("ul").children("li").each(function(index, element){
                jQuery(element).attr("data-page", i);
                i++;
            });

            jQuery(tabs).children("ul").children("li").click(function(){
                showPage(parseInt($(this).attr("data-page")));
            });
        };
        return this.each(createTabs);
    };


    function castParallax() {

        var opThresh = 350;
        var opFactor = 750;

        window.addEventListener("scroll", function (event) {

            var top = this.pageYOffset;

            var layers = document.getElementsByClassName("elem");
            var layer, speed, yPos;
            for (var i = 0; i < layers.length; i++) {
                layer = layers[i];
                speed = layer.getAttribute('data-speed');
                var yPos = -(top * speed / 100);
                layer.setAttribute('style', 'transform: translate3d(0px, ' + yPos + 'px, 0px)');

            }
        });
    }


    document.body.onload = castParallax();

})(jQuery);
