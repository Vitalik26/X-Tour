<?php

/*Template Name: home-page*/

get_header();

?>


    <div id="slider-section">
 <div class="hidden-xs dotsCont">
  <div><img src="/wp-content/themes/default/assets/images/dots1.png"/></div>
  <div><img src="/wp-content/themes/default/assets/images/dots2.png"/></div>
  <div><img src="/wp-content/themes/default/assets/images/dots3.png"/></div>
</div>
<div id="slider" class="owl-carousel">
  <div><img class="slider_1" src="/wp-content/themes/default/assets/images/slide1.jpg" />
      <div class="content_slider">
          
          <span class="">ПОХОД</span>
          <h2 class="">Неистовая<br>Говерла</h2>
          <span class="bold">3 дня | <span style="color: #00a1ec">22.07. - 24.07</span></span>

      </div>
          
      
  </div>
  <div><img class="slider_1" src="/wp-content/themes/default/assets/images/slide1.jpg" />
      <div class="content_slider">
          
          <span class="">ПОХОД</span>
          <h2 class="">Неистовая<br>Говерла</h2>
          <span class="bold">3 дня | <span style="color: #00a1ec">22.07. - 24.07</span></span>

      </div>
          
      
  </div>
  <div><img class="slider_1" src="/wp-content/themes/default/assets/images/slide1.jpg" />
      <div class="content_slider">
          
          <span class="">ПОХОД</span>
          <h2 class="">Неистовая<br>Говерла</h2>
          <span class="bold">3 дня | <span style="color: #00a1ec">22.07. - 24.07</span></span>

      </div>
          
      
  </div>
 
</div>


<script>
jQuery(document).ready(function(){
  jQuery("#slider").owlCarousel({      
    loop:true,
    items:1,
    nav:true, 
    dotData:true,
    dotsContainer: '.dotsCont',
  navText: ['<img src="/wp-content/themes/default/assets/images/prev.png" />','<img src="/wp-content/themes/default/assets/images/next.png" />']
     });});
     </script>
</div>


<div id="direct-section" >
   <h2 class="wow fadeInDown"  data-wow-delay="0.3s">Направления</h2>
     <div class="container">
            <div class="row" >
                <div data-wow-delay="0s" class="col-md-3 wow fadeInUp"><img src="/wp-content/themes/default/assets/images/direct_1.png" /><div class="img_content">Походы входногодня в Карпаты</div></div>
                <div data-wow-delay="0.3s" class="col-md-3  wow fadeInUp"><img src="/wp-content/themes/default/assets/images/direct_2.png" /><div class="img_content">Сплавы</div></div>
                <div data-wow-delay="0.6s" class="col-md-3  wow fadeInUp"><img src="/wp-content/themes/default/assets/images/direct_3.png" /><div class="img_content">Пешие туры в Карпаты для новичков</div></div>
                <div data-wow-delay="0.9s" class="col-md-3  wow fadeInUp"><img src="/wp-content/themes/default/assets/images/direct_4.png" /><div class="img_content">Экстремальные туры</div></div>

            </div>
    </div>
</div>

<div id="event-section" >
   <h2>События</h2>



     <div  class="wow fadeInUp container"  data-wow-delay="0.3s">
            <div class="owl-carousel" id="slider_event">
                <?php

                global $post;

                $args = array(
                    'posts_per_page' => -1,
                    'post_type' => 'tours'
                );

                $myposts = get_posts($args);
                foreach ($myposts as $post) : setup_postdata($post); ?>

                    <div class="cont_event "><div class="event"><a href="<?php echo get_permalink($single_post_1->ID); ?>"><div class="img_content">
                                    <div class="city"><?php  echo get_field('address_event'); ?></div><img src="<?php echo get_the_post_thumbnail_url() ?>" alt="">
                                </div><p class="black"><?php the_title(); ?></p><span>
                                      <?php
                                      $counter = 0;
                                      if(get_field('when_going_on_tours' )): ?>
                                          <?php while(has_sub_field('when_going_on_tours' )): ?>
                                              <?php
                                              $date = get_sub_field('date');
                                              if($counter >= 1){
                                                  ?> | <?php
                                              }
                                               echo $date;
                                              ?>

                                          <?php $counter++; endwhile; ?>
                                      <?php endif; ?>
                                </span></a></div></div>

                <?php endforeach;
                wp_reset_postdata(); ?>
            </div>
<script>
jQuery(document).ready(function(){
  jQuery("#slider_event").owlCarousel({      
    loop:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    },
    nav:true, 
    navText: ['<img src="/wp-content/themes/default/assets/images/prev_3.png" />','<img src="/wp-content/themes/default/assets/images/next_3.png" />']
     });});</script>
    </div>
</div>



<?php
get_template_part( 'template-parts/page/content', 'front-page' );
?>



<div id="gid-section">
   <h2>Гиды</h2>
      <div class="container">
           <div class="row wow fadeInRight container"    >
             <div class="owl-carousel" id="slider_gid">
                <div class="cont_kit" >
                     <div class="col-md-4 col-sm-4 img_grid wow fadeInLeft" data-wow-delay="1.4s"><img src="/wp-content/themes/default/assets/images/gid_1.jpg" /></div>
                     <div class="col-md-8 col-sm-8 cont_grid wow fadeInRight" data-wow-delay="2.4s">
                         <div class="img_content">
                             <h3 class="black">Аня Смоктуновская</h3>
                            <p class=" ">Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца.</p>
                            <div class="exp">Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов.</div>
                        </div></div>
                 
                </div> 
                <div class="cont_kit" >
                     <div class="col-md-4 col-sm-4 img_grid"><img src="/wp-content/themes/default/assets/images/gid_1.jpg" /></div>
                     <div class="col-md-8 col-sm-8 cont_grid">
                         <div class="img_content">
                             <h3 class="black">Аня Смоктуновская</h3>
                            <p class=" ">Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца.</p>
                            <div class="exp">Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов.</div>
                        </div></div>
                 
                </div> 
            </div>
         </div>
         
 <script>
jQuery(document).ready(function(){
   // jQuery(".cont_kit .img_grid").height(jQuery(".cont_kit .cont_grid").height());
  jQuery("#slider_gid").owlCarousel({      
    loop:true,
    items:1,
    nav:true, 
    navText: ['<img src="/wp-content/themes/default/assets/images/prev_2.png" />','<img src="/wp-content/themes/default/assets/images/next_2.png" />']
     });});</script>
    </div>
</div>

<?php
get_template_part( 'template-parts/page/content', 'front-page-panels' );
?>

<div id="press-section" >
   <h2>Пресса о нас</h2>
     <div class="container">
            <div class="row " >
                <div class="owl-carousel" id="slider_press">
                <div class="cont_press">
                     
                    <div class="img_content">
                        <div class="img_d"><img class="img_post" src="/wp-content/themes/default/assets/images/press_1.jpg" /></div>
                        <h3 class="black">Тема статьи</h3>
                        <p >Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. </p>
                        <img class="logo hidden-xs hidden-sm" src="/wp-content/themes/default/assets/images/press_logo.jpg" />
                           
                    </div>
                    
                </div> 
                <div class="cont_press">
                     
                    <div class="img_content">
                        <div class="img_d"><img class="img_post" src="/wp-content/themes/default/assets/images/press_1.jpg" /></div>
                        <h3 class="black">Тема статьи</h3>
                        <p >Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. </p>
                        <img class="logo hidden-xs hidden-sm" src="/wp-content/themes/default/assets/images/press_logo.jpg" />
                           
                    </div>
                    
                </div> 
                    
            </div>
    </div>
 <script>
jQuery(document).ready(function(){
  jQuery("#slider_press").owlCarousel({      
    loop:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:2
        }
    },
      margin:20,
    nav:true, 
    navText: ['<img src="/wp-content/themes/default/assets/images/prev_2.png" />','<img src="/wp-content/themes/default/assets/images/next_2.png" />']
     });});</script> 
   
</div>
</div>



<?php get_footer();
