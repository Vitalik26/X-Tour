
jQuery(document).ready(function(){

     //contact form send hidden field url and date page
    jQuery('.book .reservation').on('click' , function () {
        var date = jQuery('.swiper-slide-active .date_tur .black').text();
        jQuery('.hidden_date').val(date);
    });
    var loc = window.location.href;
    jQuery('.hidden_url').attr("value" , loc );



    jQuery('#direct-section2 .tabs ul li').click(function(){
        var tab_id = jQuery(this).attr('data-tab');

        jQuery('#direct-section2 .tabs ul li').removeClass('active');
        jQuery('#direct-section2 .tab-content').removeClass('current');

        jQuery(this).addClass('active');
        jQuery("#direct-section2 #"+tab_id).addClass('current');
    });


    jQuery('#ready-section .tabs ul li').click(function(){
        var tab_id = jQuery(this).attr('data-tab');

        jQuery('#ready-section .tabs ul li').removeClass('active');
        jQuery('#ready-section .tab-content').removeClass('current');

        jQuery(this).addClass('active');
        jQuery("#ready-section #"+tab_id).addClass('current');
    });


    jQuery(function(){

        // jQuery('#main').css('padding-top', jQuery('#menu-header').height())
        if(jQuery(window).height()<jQuery('.contact-section .container').height()) {

            jQuery('#map').height( jQuery('.contact-section .container').height()*1.1) ;

        } else {
            jQuery('#map').height( jQuery(window).height()*1.1 );

        }

        jQuery(window).resize(function() {
            // jQuery('#main').css('padding-top', jQuery('#menu-header').height())
            if(jQuery(window).height()<jQuery('.contact-section .container').height()) {

                jQuery('#map').height( jQuery('.contact-section .container').height()*1.1) ;

            } else {
                jQuery('#map').height( jQuery(window).height()*1.1 );

            }
        })
    });
    jQuery('.hide').hide();

    jQuery('.see_cart').on('click', function() {

        jQuery('.contact-section').slideUp(500);
        jQuery('.see_contact').slideDown(600);


    });

    jQuery('.see_contact').on('click', function() {
        jQuery('.see_contact').hide();
        jQuery('.contact-section').slideDown(500);


    });
    var swiper_date = new Swiper('.swiper-container-tour-date', {
        direction: 'vertical',
        effect: 'cube',
        // loop :true,
        allowTouchMove: false,
        // pagination: {
        //     el: '.swiper-pagination',
        //     clickable: true,
        //
        // },
        navigation: {
            nextEl: '.swiper-button-next1',
            prevEl: '.swiper-button-prev1',
        },
    });

    var swiper_date2 = new Swiper('.swiper-container-tour-date2', {
        direction: 'vertical',
        effect: 'cube',
        // loop :true,
        allowTouchMove: false,
        // pagination: {
        //     el: '.swiper-pagination',
        //     clickable: true,
        //
        // },
        navigation: {
            nextEl: '.swiper-button-next12',
            prevEl: '.swiper-button-prev12',
        },
    });


    function MainSliderSetNewInfo() {
        var price = jQuery('.swiper-container-tour-date .swiper-slide-active .date_tur .black').attr('data-number-seats');
        jQuery('.event_info .free_m span').text(price);
    }
    MainSliderSetNewInfo();

    swiper_date.on('slideNextTransitionStart', function () {
        MainSliderSetNewInfo();
    });
    swiper_date.on('slidePrevTransitionStart', function () {
        MainSliderSetNewInfo();
    });


    function MainSliderSetNewInfo2() {
        var price = jQuery('.swiper-container-tour-date2 .swiper-slide-active .date_tur .black').attr('data-number-seats');
        jQuery('.event_info .free_m span').text(price);
    }
    MainSliderSetNewInfo2();

    swiper_date2.on('slideNextTransitionStart', function () {
        MainSliderSetNewInfo2();
    });
    swiper_date2.on('slidePrevTransitionStart', function () {
        MainSliderSetNewInfo2();
    });






    var swiper = new Swiper('.swiper-container-gallery', {
        slidesPerView: 4,
        // centeredSlides: true,
        loop : true,

        navigation: {
            nextEl: '.swiper-button-nextG',
            prevEl: '.swiper-button-prevG',
        },
        breakpoints: {
            400: {
                slidesPerView: 1,
            },
            560: {
                slidesPerView: 2,
            },
            991: {
                slidesPerView: 3,
            }
        }
    });

    document.querySelectorAll('a[href^="#"]').forEach(function (anchor) {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();

            document.querySelector(this.getAttribute('href')).scrollIntoView( {
                behavior: 'smooth',
                block : "start"

            });
        });
    });

});

(function(jQuery){
    jQuery.fn.lightTabs = function(options){

        var createTabs = function(){
            tabs = this;
            i = 0;

            showPage = function(i){
                jQuery(tabs).children("div").children("div").hide();
                jQuery(tabs).children("div").children("div").eq(i).show();
                jQuery(tabs).children("ul").children("li").removeClass("active");
                jQuery(tabs).children("ul").children("li").eq(i).addClass("active");
            }

            showPage(0);

            jQuery(tabs).children("ul").children("li").each(function(index, element){
                jQuery(element).attr("data-page", i);
                i++;
            });

            jQuery(tabs).children("ul").children("li").click(function(){
                showPage(parseInt($(this).attr("data-page")));
            });
        };
        return this.each(createTabs);
    };


    function castParallax() {

        var opThresh = 350;
        var opFactor = 750;

        window.addEventListener("scroll", function (event) {

            var top = this.pageYOffset;

            var layers = document.getElementsByClassName("elem");
            var layer, speed, yPos;
            for (var i = 0; i < layers.length; i++) {
                layer = layers[i];
                speed = layer.getAttribute('data-speed');
                var yPos = -(top * speed / 100);
                layer.setAttribute('style', 'transform: translate3d(0px, ' + yPos + 'px, 0px)');

            }
        });
    }


    castParallax();

})(jQuery);

jQuery(document).ready(function () {
    jQuery('div.dotsCont div').on('click', function () {
        switchSlider(jQuery(this).data('sliderid'), jQuery(this));
    });

    var timer1;
    var timer2;
    var timer3;

    function switchSlider(id, selected_element) {
        clearTimeout(timer1);
        clearTimeout(timer2);
        clearTimeout(timer3);


        jQuery('div.dotsCont div').each(function () {
            jQuery(this).removeClass('active');
        });
        jQuery(selected_element).addClass('active');

        jQuery('#slider-section .home_main_slider').each(function () {
            var slider = jQuery(this);
            jQuery(this).css('opacity', '0');
            timer1 = setTimeout(function () {
                jQuery(slider).css('display', 'none');
            }, 500)
        });

        timer2 = setTimeout(function () {
            jQuery('#slider-section .home_main_slider#' + id).css('display', 'block');
        }, 500);

        timer3 = setTimeout(function () {
            jQuery('#slider-section .home_main_slider#' + id).css('opacity', '1');
        }, 900);
    }



    jQuery('.grid3').on('click' , function () {
        console.log('cl')
        jQuery('#programm-section .row.row_posts .cont_event').each(function () {
            jQuery(this).removeClass('col-md-6');
            jQuery(this).addClass('col-md-4');
            jQuery('#programm-section .img_content').css('height', '165px');
        });
    });

    jQuery('.grid2').on('click' , function () {
        console.log('cl')
        jQuery('#programm-section .row.row_posts .cont_event').each(function () {
            jQuery(this).removeClass('col-md-4');
            jQuery(this).addClass('col-md-6');
            jQuery('#programm-section .img_content').css('height', '221px');
        });
    });






});

jQuery(document).ready(function () {
    jQuery('#calendar').DataTable({
        info: false,
        searching: false,
        pageLength: 20,
        lengthChange: false,
        pagingType: 'numbers'
    });
});

