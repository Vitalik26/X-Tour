<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package x-Tour
 */

get_header();
?>
    <div class="programm_b_h">
        <h2>Программы событий</h2>
        <!--            <form>-->
        <!--                <input type="text" name="city" class="city" placeholder="Город" />-->
        <!--                <input type="text" name="date" class="date" placeholder="Дата" />-->
        <!--            </form>-->
    </div>


    <div id="programm-section">

        <div class="container">
            <div class="row napr">
                <h3>Направление:</h3>
                <div>
                <a class="" href="<?php echo get_permalink(7); ?>">Все туры</a>
                <?php
//                the_archive_title();
//                the_archive_description('<div class="archive-description">', '</div>');
                ?>
                <?php
                $taxonomy = 'tour_categories';
                $terms = get_terms($taxonomy);
                $id_current_taxonomy = get_queried_object()->term_id;
                if ($terms && !is_wp_error($terms)) :
                    ?>
                    <?php foreach ($terms as $term) {?>
                    <a class="<?php if ($term->term_id == $id_current_taxonomy) echo "active"; ?>" href="<?php echo get_term_link($term->slug, $taxonomy); ?>"><?php echo $term->name; ?></a>
                <?php } ?>

                <?php endif; ?>
            </div>
                <div class="wrap_grid_filter">
                    <img class="grid3" src="<?php echo get_template_directory_uri(); ?>/assets/images/menu3.png" alt="">
                    <img class="grid2" src="<?php echo get_template_directory_uri(); ?>/assets/images/grid2.png" alt="">
                </div>
            </div>
            <div class="row row_posts">
            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <div class="cont_event col-md-6">
                    <div class="event"><a href="<?php echo get_permalink($post->ID); ?>">
                            <div class="img_content">
                                <div class="city"><?php echo get_field('address_event', $post->ID) ?></div>
                                <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="">
                            </div>
                            <p class="black"><?php the_title(); ?></p></a>

                        <span><?php
                            $counter = 0;
                            if (get_field('dates', $post->ID)):
                                while (has_sub_field('dates', $post->ID)):
                                    $start_date = get_sub_field('start_date', $post->ID);
                                    $end_date = get_sub_field('end_date', $post->ID);
                                    if (get_row_index() == 4) break;
                                    if ($counter >= 1) {
                                        ?><span class="devider">|</span><?php
                                    }
                                ;
                                    ?>
                                              <a href="<?php echo get_permalink($post->ID) ?>"><?php echo $start_date ?>
                                    - <?php echo $end_date ?></a>';
                                    <?php $counter++; endwhile;
                            endif; ?>
                                       </span>
                    </div>
                    </div><?php

                endwhile;
            else :
            endif;
            ?>
            </div>
        </div>
    </div>

<?php
get_template_part('template-parts/page/content', 'front-page');
get_template_part('template-parts/page/content', 'page');
?>
    </div>
<?php get_template_part('template-parts/page/slider-swiper-gallery', 'page'); ?>

<?php
get_template_part('template-parts/page/content', 'front-page-panels');
?>

    <script type="text/javascript">
        jQuery(document).ready(function (jQuery) {
            // This is required for AJAX to work on our page
            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

            function cvf_load_all_posts(page) {
                // Start the transition
                jQuery("#programm-section .container .row_posts").fadeIn().css({"background-color": "ccc"});
                // Data to receive from our server
                // the value in 'action' is the key that will be identified by the 'wp_ajax_' hook
                var data = {
                    page: page,
                    action: "demo-pagination-load-posts"
                };
                // Send the data
                jQuery.post(ajaxurl, data, function (response) {
                    // If successful Append the data into our html container

                    jQuery("#programm-section .container .row_posts").html(response);
                    // console.log(response);
                    // Handle the clicks
                    jQuery('span.active').on('click', function () {
                        var page = jQuery(this).attr('p');
                        cvf_load_all_posts(page);
                    });
                    // End the transition
                    jQuery("#programm-section .container .row_posts ").css({
                        'background': 'none',
                        'transition': 'all 1s ease-out'
                    });
                });
            }

            // Load page 1 as the default
            cvf_load_all_posts(1);
        });
    </script>

<?php
get_footer();
