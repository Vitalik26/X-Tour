<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>


<div id="email-section" >
      <div class="container">
            <div class="row " >
                <div class="col-md-5" style=" padding: 0;"><h3>Подписаться на рассылку</h3></div>
<!--            --><?php //echo do_shortcode('[mc4wp_form]') ?>
                <div class="col-md-4"><input type="email" class="input" placeholder="E-mail"></div>
                <div class="col-md-3"><input type="submit" class="submit-btn" value="Подписаться"></div>
                
            </div>

</div>
</div>

<div id="footer-section" >
      <div class="container">
            <div class="row " >
                <div class="col-md-3 col-sm-6" style=" padding: 0;">
                    <?php if(get_field('addresses' , 'option')): ?>
                        <?php while(has_sub_field('addresses' , 'option')): ?>
                         <?php $address = get_sub_field('address' , 'option');
                            ?>

                          <h3>Адрес</h3><p><?php echo $address; ?></p></div>

                        <?php endwhile; ?>
                    <?php endif; ?>

                <div class="col-md-3 col-sm-6 footer_border" >
                    <div class="social">
                        <?php if(get_field('social' , 'option')): ?>
                            <?php while(has_sub_field('social' , 'option')): ?>
                                <?php $image = get_sub_field('image', 'option');
                                $link = get_sub_field('link' , 'option');
                                ?>
                                <a href="<?php echo $link; ?>" class=""><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" /></a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    <div class="phone black">
                        <?php if(get_field('phones' , 'option')): ?>
                            <?php while(has_sub_field('phones' , 'option')): ?>
                                <a style="color: white" href="tel:<?php echo get_sub_field('phone_part1red' , 'option') ?> <?php echo get_sub_field('phone_part2white' , 'option') ?>""><span style="color: #ed3f1c"><?php echo get_sub_field('phone_part1red' , 'option') ?></span><?php echo get_sub_field('phone_part2white' , 'option') ?></a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>


                </div>
                <div class="col-md-6 col-sm-12 cal_bl">
                     <div class="row_f">
                        <ul class="top-right-menu">
                            <?php wp_nav_menu( array( 'default' => 'footer_header_menu' ) ); ?>
                        </ul>
                     
                      </div>
                     <div class="row_f top-right-bottom">
                         <?php if(get_field('custom_menu_under_the_main_menu' , 'option')): ?>
                             <?php while(has_sub_field('custom_menu_under_the_main_menu' , 'option')): ?>
                                 <?php $image = get_sub_field('img', 'option');
                                 $title_page = get_sub_field('title_page' , 'option');
                                 $link = get_sub_field('link_to_page' , 'option');
                                 ?>
                                 <div style="margin-right: 25px;"><a href="<?php echo $link; ?>"><img style="height: 17px;"  src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>"/><?php echo  $title_page; ?></a></div>
                             <?php endwhile; ?>
                         <?php endif; ?>

                     </div>
                </div>

            </div>
</div>
</div>

</div>

 
<!--<script src="/wp-content/themes/default/assets/js/bootstrap.min.js"></script>-->



<?php 				///get_template_part( 'template-parts/footer/footer', 'widgets' );
 wp_footer(); ?>

</body>
</html>
