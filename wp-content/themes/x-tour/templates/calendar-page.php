<?php
/*
   Template Name: calendar-page
   */

get_header(); ?>
    <div id="calendar_page">
        <div class="programm_b_h">
            <h2>Программы событий</h2>
            <form>
                <input type="text" name="city" class="city" placeholder="Город"/>
                <input type="text" name="date" class="date" placeholder="Дата"/>
            </form>

        </div>
        <div id="programm-section">

            <div class="container">
                <div class="row napr" >
                    <h3>Направление:</h3>
                    <?php global $post;
                    $post_slug=$post->post_name;
                    ?>

                    <a class="<?php if (!$_GET['type']) echo "active"; ?>" href="<?php echo get_permalink( get_page_by_path( 'calendar-2' ) ); ?>">Все туры</a>
                    <?php
                    $taxonomy = 'tour_categories';
                    $terms = get_terms($taxonomy);
                    if ( $terms && !is_wp_error( $terms ) ) :
                        ?>
                        <?php foreach ( $terms as $term ) { ?>
                        <a class="<?php if ($_GET['type'] == $term->slug ) echo "active"; ?>" href="<?php echo add_query_arg('type', $term->slug, get_permalink()); ?>"><?php echo $term->name; ?></a>
                    <?php } ?>

                    <?php endif;?>



                </div>

                <div class="row table">
                    <table id="calendar" class="display" style="width:100%">
                        <thead>
                        <tr>
                            <th>Дата</th>
                            <th>Название</th>
                            <th>Продолж-сть</th>
                            <th>Место</th>
                            <th>Растояние</th>
                            <th>Сложность</th>
                            <th>Цена</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        $type = '';
                        if ($_GET['type']) {
                            $type = $_GET['type'];
                        }
                        $args = array(
                            'post_type' => 'tours',
                            'posts_per_page' => -1,
                            'tour_categories'    => $type,
                            //    'order'    => 'ASC'
                        );

                        $the_query = new WP_Query($args);
                        $posts = $the_query->posts;
                        foreach ($posts as $single_post) {
                            $id = $single_post->ID;
                            $nearest_date = get_field('dates', $id)[0];
                            $term = wp_get_post_terms($id, 'tour_categories')[0];

                            $counter = 0;
                            if (have_rows('Program_our', $id)):
                                while (have_rows('Program_our', $id)): the_row();
                                    $counter++;
                                endwhile;
                            endif; ?>


                            <tr>
                                <td><?php echo $nearest_date['start_date'] ?> - <?php echo $nearest_date['end_date'] ?></td>
                                <td style="text-align: left;">
                                    <a href="<?php echo $single_post->guid ?>">
                                        <img class="tour_type" src="<?php echo get_field('img', $term) ?>" alt="">
                                        <?php echo $single_post->post_title ?>
                                    </a>
                                </td>
                                <td><?php echo $counter ?> <?php
                                    if ($counter == 0 || $counter > 4) {
                                        echo 'дней';
                                    } else if ($counter == 1) {
                                        echo 'день';
                                    } else {
                                        echo 'дня';
                                    }
                                    ?></td>
                                <td><?php echo get_field('address_event', $id) ?></td>
                                <td><?php echo get_field('distance', $id)['number'] ?> <?php echo get_field('distance', $id)['unit'] ?></td>
                                <td><?php echo get_field('difficulty', $id) ?></td>
                                <td><?php echo get_field('tour price', $id) ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>


                <div class="dotsCont">
                    <div>1</div>
                    <div>2</div>
                    <div>3</div>
                    <div>4</div>
                    <div>5</div>
                    <div>6</div>
                    <div>7</div>
                    <div>8</div>
                </div>
            </div>
        </div>
    </div>
<?php
get_template_part('template-parts/page/content', 'front-page');
get_template_part('template-parts/page/content', 'page');
?>

<?php get_template_part('template-parts/page/slider-swiper-gallery', 'page'); ?>

<?php
get_template_part('template-parts/page/content', 'front-page-panels');
?>


<?php get_footer();
