<?php
 /*
    Template Name: directions-page
    */
get_header(); ?>
    <script type="text/javascript">
        jQuery(document).ready(function(jQuery) {
            // This is required for AJAX to work on our page
            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
            function cvf_load_all_posts(page){
                // Start the transition
                jQuery("#programm-section .container .row_posts").fadeIn().css({"background-color": "ccc"});
                // Data to receive from our server
                // the value in 'action' is the key that will be identified by the 'wp_ajax_' hook
                var data = {
                    page: page,
                    action: "demo-pagination-load-posts"
                };
                // Send the data
                jQuery.post(ajaxurl, data, function(response) {
                    // If successful Append the data into our html container

                    jQuery("#programm-section .container .row_posts").html(response);
                        jQuery('#programm-section .container .row_posts').removeClass('row_start_remove')
                    // console.log(response);
                    // Handle the clicks
                    jQuery('span.active').on('click',function(){
                        var page = jQuery(this).attr('p');
                        cvf_load_all_posts(page);
                    });
                    // End the transition
                    jQuery("#programm-section .container .row_posts ").css({'background':'none', 'transition':'all 1s ease-out'});
                });
            }
            // Load page 1 as the default
            cvf_load_all_posts(1);
        });
    </script>
           <div class="programm_b_h" >
            <h2>Программы событий</h2>
<!--            <form>-->
<!--                <input type="text" name="city" class="city" placeholder="Город" />-->
<!--                <input type="text" name="date" class="date" placeholder="Дата" />-->
<!--            </form>-->
         </div>


<div id="programm-section"  >

    <div class="container">
        <div class="row napr" >
            <div>
            <h3>Направление:</h3>
            <?php $page_id = get_the_ID()?>

            <a class="<?php if (7 == $page_id) echo "active"; ?>" href="<?php echo get_permalink(7); ?>">Все туры</a>
            <?php
            $taxonomy = 'tour_categories';
            $terms = get_terms($taxonomy);
            if ( $terms && !is_wp_error( $terms ) ) :
                ?>
                    <?php foreach ( $terms as $term ) { ?>
<!--                      <a href="">--><?php //echo $term->name; ?><!--</a>-->
                      <a href="<?php echo get_term_link($term->slug, $taxonomy); ?>"><?php echo $term->name; ?></a>
                    <?php } ?>

            <?php endif;?>

            </div>
            <div class="wrap_grid_filter">
                <img class="grid3" src="<?php echo get_template_directory_uri(); ?>/assets/images/menu3.png" alt="">
                <img class="grid2" src="<?php echo get_template_directory_uri(); ?>/assets/images/grid2.png" alt="">
            </div>

         </div>

        <div class="row row_posts row_start_remove"><!-- append here post-->  <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div></div>

    </div>
</div>

<?php
get_template_part( 'template-parts/page/content', 'front-page' );
get_template_part( 'template-parts/page/content', 'page' );
?>
</div>

<!--<div id="slider-photos">-->
<!--<div class="owl-carousel ">-->
<!--    <div class="item"><img src="/wp-content/themes/default/assets/images/sl-1.jpg" /></div>-->
<!--    <div class="item"><img src="/wp-content/themes/default/assets/images/sl-2.jpg" /></div>-->
<!--    <div class="item"><img src="/wp-content/themes/default/assets/images/sl-3.jpg" /></div>-->
<!--    <div class="item"><img src="/wp-content/themes/default/assets/images/sl-4.jpg" /></div>-->
<!---->
<!--</div>-->
<!--   <script>-->
<!--    jQuery(document).ready(function(){-->
<!--    jQuery("#slider-photos .owl-carousel").owlCarousel({-->
<!--    loop:true,    autoWidth:true,-->
<!---->
<!-- items:4,-->
<!-- nav: true,-->
<!--     navText: ['<img src="/wp-content/themes/default/assets/images/prev_3.png" />','<img src="/wp-content/themes/default/assets/images/next_3.png" />']-->
<!--   });});</script>-->
<!--</div>-->
<?php    get_template_part( 'template-parts/page/slider-swiper-gallery', 'page' ); ?>

<?php
get_template_part( 'template-parts/page/content', 'front-page-panels' );
?>


<?php get_footer();
