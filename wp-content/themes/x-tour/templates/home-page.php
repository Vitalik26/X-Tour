<?php

/*Template Name: home-page*/

get_header();

?>


<div id="slider-section" class="home_page">
    <!--        <div class="hidden-xs dotsCont">-->
    <!--            <div><img src="/wp-content/themes/default/assets/images/dots1.png"/></div>-->
    <!--            <div><img src="/wp-content/themes/default/assets/images/dots2.png"/></div>-->
    <!--            <div><img src="/wp-content/themes/default/assets/images/dots3.png"/></div>-->
    <!--        </div>-->
    <!--<div class="hidden-xs dotsCont">
        <?php
    /*        // check if the repeater field has rows of data
            if (have_rows('main_slider')):
                // loop through the rows of data
                while (have_rows('main_slider')) : the_row();
                    // display a sub field value
                    */ ?>
                <div style="background-color: <?php /*echo the_sub_field('title_background'); */ ?>"
                     data-sliderId="slider_<?php /*echo get_row_index(); */ ?>"
                     class="<?php /*echo (get_row_index() == '1') ? 'active' : '' */ ?>">
                    <span><?php /*echo the_sub_field('title'); */ ?></span></div>
                <?php
    /*            endwhile;
            else :
                // no rows found
            endif;

            */ ?>
    </div>-->

    <?php
    if (have_rows('main_slider')):
        $slider_number = 1;
        while (have_rows('main_slider')) :
            the_row();
            if ($slider_number <= 1) {
                ?>
                <div id="slider_<?php echo get_row_index(); ?>"
                     class="owl-carousel home_main_slider"
                    <?php echo (get_row_index() == '1') ? 'style="display: block";' : 'style="display: none";' ?>>
                    <?php
                    if (have_rows('slide')):
                        while (have_rows('slide')) : the_row();
                            $id = get_sub_field('tour');
                            $temp = get_post($id);
                            $type = get_the_terms($id, 'tour_categories')[0]->name;

                            $video_link = get_sub_field('video');

                            $dates = get_field('dates', $id)[0];
                            $start_date = $dates['start_date'];
                            $end_date = $dates['end_date'];

                            $counter = 0;
                            if (have_rows('Program_our', $id)):
                                while (have_rows('Program_our', $id)): the_row();
                                    $counter++;
                                endwhile;
                            endif; ?>

                            <?php if ($video_link) {
                                ?>
                                <div>
                                    <?php
                                    preg_match('/src="(.+?)"/', $video_link, $matches);
                                    $src = $matches[1];
                                    $params = array(
                                        'controls' => 0,
                                        'hd' => 1,
                                        'autohide' => 1,
                                        'rel' => 0
                                    );
                                    $new_src = add_query_arg($params, $src);
                                    $video_link = str_replace($src, $new_src, $video_link);
                                    $attributes = 'frameborder="0"';
                                    $video_link = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $video_link);
                                    echo $video_link;
                                    ?>
                                </div>
                                <?php
                            } else { ?>

                                <a href="<?php echo get_permalink($id) ?>">
                                    <img class="slider_1"
                                         src="<?php echo get_the_post_thumbnail_url($id) ?>"/>
                                    <div class="content_slider elem" data-speed="-25">

                                        <span class=""><?php echo $type ?></span>
                                        <h2 class=""><?php echo $temp->post_title ?></h2>
                                        <span class="bold"><?php echo $counter ?> <?php
                                            if ($counter == 0 || $counter > 4) {
                                                echo 'дней';
                                            } else if ($counter == 1) {
                                                echo 'день';
                                            } else {
                                                echo 'дня';
                                            }
                                            ?> | <span style="color: #00a1ec"><?php echo $start_date ?>
                                                - <?php echo $end_date ?></span></span>
                                    </div>
                                </a>
                            <?php }
                        endwhile;
                    endif;
                    ?>
                </div>
                <script>
                    jQuery(document).ready(function () {
                        jQuery("#slider_<?php echo get_row_index(); ?>").owlCarousel({
                            loop: true,
                            items: 1,
                            nav: true,
                            dotData: true,
                            video: true,
                            lazyLoad: true,
                            merge: true,
                            mouseDrag: false,
                            onChanged: function () {
                                jQuery(document).ready(function () {
                                    jQuery('.home_main_slider iframe').each(function () {
                                        if (jQuery(this).attr('src').indexOf('&autoplay=1') !== -1) {
                                            var link =jQuery(this).attr('src');
                                            var index = link.indexOf('&autoplay=1');
                                            var new_link = link.substr(0, index);
                                            jQuery(this).attr('src', new_link);
                                        }

                                    });
                                    setTimeout(function() {
                                        jQuery(".home_main_slider .active iframe")[0].src += "&autoplay=1";
                                    })
                                });

                            },
//                            autoHeight: true,
//                            dotsContainer: '.dotsCont',
                            navText: ['<img src="<?php echo get_template_directory_uri(); ?>/assets/images/prev.png" />',
                                '<img src="<? echo get_template_directory_uri() ?>/assets/images/next.png" />']
                        });
                    });
                </script>
            <?php }
            $slider_number++;
        endwhile;
    endif;
    ?>

</div>


<div id="direct-section">
    <h2 class="wow fadeInDown" data-wow-delay="0.3s">Направления</h2>
    <div class="container">
        <div class="row">
            <!--<div data-wow-delay="0s" class="col-md-3 wow fadeInUp"><img
                        src="/wp-content/themes/default/assets/images/direct_1.png"/>
                <div class="img_content">Походы входногодня в Карпаты</div>
            </div>
            <div data-wow-delay="0.3s" class="col-md-3  wow fadeInUp"><img
                        src="/wp-content/themes/default/assets/images/direct_2.png"/>
                <div class="img_content">Сплавы</div>
            </div>
            <div data-wow-delay="0.6s" class="col-md-3  wow fadeInUp"><img
                        src="/wp-content/themes/default/assets/images/direct_3.png"/>
                <div class="img_content">Пешие туры в Карпаты для новичков</div>
            </div>
            <div data-wow-delay="0.9s" class="col-md-3  wow fadeInUp"><img
                        src="/wp-content/themes/default/assets/images/direct_4.png"/>
                <div class="img_content">Экстремальные туры</div>
            </div>-->
            <?php
            $args = array(
                'taxonomy' => 'tour_categories',
                'hide_empty' => false,
            );
            $terms = get_terms($args);

            $counter = 0;
            $counterNumber = 1;
            foreach ($terms as $term) {
                $term_id = $term->term_id;
                ?>
                <a href="<?php echo get_term_link($term_id) ?>" data-wow-delay="0.<?php echo $counter + 2 ?>s"
                   class="col-md-3  wow fadeInUp">
                    <div>
                    <img src="<?php echo get_field('img', $term) ?>"/>
                    </div>
                    <div class="img_content"><?php echo $term->name ?></div>
                </a>
                <?php $counter++;
                $counterNumber++; ?>
            <?php } ?>

        </div>
    </div>
</div>

<div id="event-section">
    <h2>События</h2>
    <div class="wow fadeInUp container" data-wow-delay="0.3s">
<!--        <div class="owl-carousel" id="slider_event">-->

        <?php
        $type = 'tours';
        $args = array(
            'post_type' => $type,
            'post_status' => 'publish',
            'posts_per_page' => 6,
        );
        $my_query = null;
        $my_query = new WP_Query($args);
        ?>

        <?php if( $my_query->have_posts() ):?>
            <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>


                <div class="cont_event col-md-6 ">
                    <div class="event">
                        <a href="<?php echo get_permalink($id); ?>">
                            <div class="img_content">
                                <div class="city"><?php echo get_field('address_event', $id); ?></div>
                                <img src="<?php echo get_the_post_thumbnail_url($id) ?>" alt="">
                            </div>
                            <p class="black"><?php echo $temp->post_title; ?></p>

                            <span>
                                      <?php
                                      $counter = 0;
                                      if (get_field('dates', $id)): ?>
                                          <?php while (has_sub_field('dates', $id)): ?>
                                              <?php
                                              if ($counter < 3) {
                                                  $start_date = get_sub_field('start_date', $id);
                                                  $end_date = get_sub_field('end_date', $id);

                                                  if ($counter >= 1) {
                                                      ?><span class="devider">|</span><?php
                                                  }
                                                  ?>
                                                  <?php echo $start_date; ?>
                                                  - <?php echo $end_date; ?>
                                                  <?php
                                                  ?>
                                                  <?php $counter++;
                                              }
                                          endwhile;





                                     endif; ?>
                                </span>
                        </a>
                    </div>
                </div>
<?php
                if (  $my_query->max_num_pages > 1 ) : ?>
                <script id="true_loadmore">
                    var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
                    var true_posts = '<?php echo serialize($my_query->query_vars); ?>';
                    var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                </script>


                <?php endif; ?>
            <?php endwhile; ?>
        <?php
        endif;
        wp_reset_query();
        ?>




<!--            --><?php
//            if (have_rows('events')):
//                while (have_rows('events')) : the_row();
//                    $temp = get_sub_field('event');
//                    $id = $temp->ID;
//                    ?>
<!--                    <div class="cont_event col-md-6 ">-->
<!--                        <div class="event">-->
<!--                            <a href="--><?php //echo get_permalink($id); ?><!--">-->
<!--                                <div class="img_content">-->
<!--                                    <div class="city">--><?php //echo get_field('address_event', $id); ?><!--</div>-->
<!--                                    <img src="--><?php //echo get_the_post_thumbnail_url($id) ?><!--" alt="">-->
<!--                                </div>-->
<!--                                <p class="black">--><?php //echo $temp->post_title; ?><!--</p>-->
<!---->
<!--                            <span>-->
<!--                                      --><?php
//                                      $counter = 0;
//                                      if (get_field('dates', $id)): ?>
<!--                                          --><?php //while (has_sub_field('dates', $id)): ?>
<!--                                              --><?php
//                                              if ( $counter < 3 ) {
//                                                  $start_date = get_sub_field('start_date', $id);
//                                                  $end_date = get_sub_field('end_date', $id);
//
//                                                  if ($counter >= 1) {
//                                                      ?><!--<span class="devider">|</span>--><?php
//                                                  }
//                                                  ?>
<!--                                                  --><?php //echo $start_date; ?>
<!--                                                  - --><?php //echo $end_date; ?>
<!--                                                  --><?php
//                                                  ?>
<!--                                                  --><?php //$counter++;
//                                              }
//                                              endwhile; ?>
<!--                                      --><?php //endif; ?>
<!--                                </span>-->
<!--                            </a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    --><?php
//                endwhile;
//            endif; ?>
<!--        </div>-->
        <script>
            //jQuery(document).ready(function () {
            //    jQuery("#slider_event").owlCarousel({
            //        loop: true,
            //        responsive: {
            //            0: {
            //                items: 1
            //            },
            //            600: {
            //                items: 2
            //            },
            //            1000: {
            //                items: 2
            //            }
            //        },
            //        nav: true,
            //        navText: ['<img src="<?php //echo get_template_directory_uri(); ?>///assets/images/prev_3.png" />', '<img src="<?php //echo get_template_directory_uri(); ?>///assets/images/next_3.png" />']
            //    });
            // });
        </script>
    </div>

    <div class="lds-css ng-scope spinner" style="display: none"><div style="width:10%;height:10%; margin: auto;" class="lds-ellipsis"><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div></div>
    </div>
</div>


<?php
get_template_part('template-parts/page/content', 'front-page');
?>

<!---->
<!--<div id="gid-section">-->
<!--    <h2>Гиды</h2>-->
<!--    <div class="container">-->
<!--        <div class="row wow fadeInRight container">-->
<!--            <div class="owl-carousel" id="slider_gid">-->
<!--                <div class="cont_kit">-->
<!--                    <div class="col-md-4 col-sm-4 img_grid wow fadeInLeft" data-wow-delay="1.4s"><img-->
<!--                                src="--><?php //echo get_template_directory_uri() . '/assets/images/gid_1.jpg'?><!--"/></div>-->
<!--                    <div class="col-md-8 col-sm-8 cont_grid wow fadeInRight" data-wow-delay="2.4s">-->
<!--                        <div class="img_content">-->
<!--                            <h3 class="black">Аня Смоктуновская</h3>-->
<!--                            <p class=" ">Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда-->
<!--                                приемлемые модификации, например, юмористические вставки или слова, которые даже-->
<!--                                отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта,-->
<!--                                вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца.</p>-->
<!--                            <div class="exp">Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский-->
<!--                                набор слов.-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                </div>-->
<!--                <div class="cont_kit">-->
<!--                    <div class="col-md-4 col-sm-4 img_grid"><img-->
<!--                                src="--><?php //echo get_template_directory_uri() . '/assets/images/gid_1.jpg' ?><!--"/></div>-->
<!--                    <div class="col-md-8 col-sm-8 cont_grid">-->
<!--                        <div class="img_content">-->
<!--                            <h3 class="black">Аня Смоктуновская</h3>-->
<!--                            <p class=" ">Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда-->
<!--                                приемлемые модификации, например, юмористические вставки или слова, которые даже-->
<!--                                отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта,-->
<!--                                вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца.</p>-->
<!--                            <div class="exp">Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский-->
<!--                                набор слов.-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->

<script>
    jQuery(document).ready(function () {
        // jQuery(".cont_kit .img_grid").height(jQuery(".cont_kit .cont_grid").height());
        jQuery("#slider_gid").owlCarousel({
            loop: true,
            items: 1,
            nav: true,
            navText: ['<img src="<?php echo get_template_directory_uri(); ?>/assets/images/prev_2.png" />', '<img src="<?php echo get_template_directory_uri(); ?>/assets/images/next_2.png" />']
        });
    });</script>
<!--    </div>-->
<!--</div>-->

<?php
get_template_part('template-parts/page/content', 'front-page-panels');
?>

<!--<div id="press-section">
    <h2>Пресса о нас</h2>
    <div class="container">
        <div class="row ">
            <div class="owl-carousel" id="slider_press">
                <?php
/*                $posts = get_posts();
                foreach ($posts as $single_post) {
                    */ ?>
                    <a href="<?php /*echo get_permalink(); */ ?>" class="cont_press">
                        <div class="img_content">
                            <div class="img_d"><img class="img_post"
                                                    src="<?php /*echo get_the_post_thumbnail_url($single_post->ID) */ ?>"/></div>
                            <h3 class="black"><?php /*echo $single_post->post_title */ ?></h3>
                            <p><?php /*echo wp_trim_words( $single_post->post_content, 12) */ ?></p>
                            <img class="logo hidden-xs hidden-sm"
                                 src="<?php /*echo get_field('logo', $single_post->ID) */ ?>"/>

                        </div>
                    </a>
                <?php /*} */ ?>
            </div>
        </div>
        <script>
            jQuery(document).ready(function () {
                jQuery("#slider_press").owlCarousel({
                    loop: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 2
                        },
                        1000: {
                            items: 2
                        }
                    },
                    margin: 20,
                    nav: true,
                    navText: ['<img src="<?php /*echo get_template_directory_uri(); */ ?>/assets/images/prev_2.png" />', '<img src="<?php /*echo get_template_directory_uri(); */ ?>/assets/images/next_2.png" />']
                });
            });</script>

    </div>
</div>-->

<?php get_footer(); ?>
