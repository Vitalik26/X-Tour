<?php

/*Template Name: afterparty-page*/

get_header();

?>

<div id="afterparty" style="background-image: url(<?php echo get_field('main_background') ?>)">

    <div class="main_block">
        <span class="page_heading">
            <?php echo get_field('heading') ?>
        </span>
        <span class="day_address">
            <?php echo get_field('day_address') ?>
        </span>
        <span class="time">
            <?php echo get_field('time') ?>
        </span>
        <a class="book_button" data-toggle="modal" data-target="#afterparty_book">Хочу пойти!</a>
        <div class="social_links">
            <?php
            if( have_rows('social_links') ):
                while ( have_rows('social_links') ) : the_row();
                    ?>
                    <a href="<?php echo get_sub_field('link') ?>">
                        <img src="<?php echo get_sub_field('icon') ?>" alt="значок">
                    </a>
                    <?php
                endwhile;
            endif;
            ?>
        </div>
    </div>

</div>

<div class="modal fade" id="afterparty_book" tabindex="-1" role="dialog" aria-labelledby="afterparty_book" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="background-image: url(<?php echo get_field('modal_background') ?>)">
            <div class="modal-header">
<!--                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>-->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo do_shortcode('[contact-form-7 id="304" title="Afterparty"]') ?>
            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>
