<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

    <div class="wrap">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

                <?php
                /* Start the Loop */
                while (have_posts()) :
                the_post(); ?>
                <?php
                $id = get_the_ID();
                $type = get_the_terms($id, 'tour_categories')[0]->name;
                $counter = 0;
                if (have_rows('Program_our', $id)):
                    while (have_rows('Program_our', $id)): the_row();
                        $counter++;
                    endwhile;
                endif; ?>

                <div id="slider-section">
                    <div id="slider" class="single_tour_main_slider">
                        <div><img style="width: 100%" class="slider_1 "
                                  src="<?php echo get_the_post_thumbnail_url() ?>"/>
                            <div class="content_slider elem" data-speed="-25">
                                <span class=""><?php echo $type ?></span>
                                <h2 class=""><?php the_title() ?></h2>
                                <span class="bold"><?php echo $counter  ?> <?php
                                    if ($counter == 0 || $counter > 4) {
                                        echo 'дней';
                                    } else if ($counter == 1) {
                                        echo 'день';
                                    }else {
                                        echo 'дня';
                                    }
                                    ?> | <span style="color: #00a1ec">
                                        <?php $dates = get_field('dates', $id)[0];
                                        $start_date = $dates['start_date'];
                                        $end_date = $dates['end_date'];
                                        echo $start_date ?> - <?php echo $end_date
                                        ?>

                                    </span>
                                </span>

                            </div>
                            <div class="arrow_go"><a href="#arr_down_go"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/arr_down_w.png"/></a></div>

                        </div>
                    </div>

                </div>

                <style>
                    #direct-section2 .direct_bl {
                        padding-bottom: 115px;
                    }
                </style>

                <?php
                get_template_part('template-parts/page/content', 'page');
                ?>
                <div class="elem wrap_includes" data-speed="5">
                    <?php
                    get_template_part('template-parts/navigation/navigation', 'top');
                    ?>
                </div>

        </div>
<?php $imageBG = get_field('background_before_tour_program_up'); ?>
        <div id="dates-section" style="background: url(<?php echo $imageBG['url']; ?>); background-size: cover;">
            <h2 class="elem" data-speed="5"><?php echo get_field('Title_coming_dates') ?></h2>
            <div class="container flex elem" data-speed="5">
                <div class="row flex">
                    <div class="col-md-3 date_block">
                        <div class="swiper-button-prev12" href="#"><img
                                    src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow_up.png"/>Предыдущий
                        </div>

                        <div class="swiper-container swiper-container-tour-date2" style="height: 130px">
                            <div class="swiper-wrapper">
                                <?php if (have_rows('dates')): ?>
                                    <?php while (have_rows('dates')): the_row(); ?>
                                        <?php
                                        $start_date =  get_sub_field('start_date');
                                        $end_date =  get_sub_field('end_date');
                                        $number_seats =  get_sub_field('number_seats');
                                        ?>
                                        <div class="swiper-slide" style="cursor: pointer">
                                            <div class="date_tur"><span>Дата тура</span>
                                                <h3 data-number-seats="<?php echo $number_seats ?>" class="black"><?php echo $start_date ?> - <?php echo $end_date?></h3></div>
                                        </div>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                        </div>


                        <div class="swiper-button-next12" href="#">Следующий<img
                                    src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow_down.png"/></div>
<!--                        <div class="calendar_icon"><img-->
<!--                                    src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/date-cal_icon.png"/></div>-->
                    </div>
                    <div class="col-md-9 event_bl">
                        <div class="event_info">
                            <div class="free_m"><?php echo get_field('title_total_number') ?>
                                <span></span></div>
                            <h2 class="black"><?php the_title(); ?></h2>
                            <p><?php
                                $content = get_the_content();
                                echo wp_trim_words($content, 18, '...'); ?></p>
                            <div class="col-md-6 price black"><?php echo get_field('tour price') ?></div>
                            <div class="col-md-6 book"><a data-toggle="modal"
                                                          data-target="#exampleModalCenterReservation" href="#"
                                                          class="b reservation">Забронировать место<img
                                            src="<?php echo get_template_directory_uri(); ?>/assets/images/next_1.png"/></a></div>
                        </div>

                    </div>
                </div>
            </div>

            <div id="call-section" style=" background: url(<?php echo get_field('background_before_tour_program_down')['url'] ?>);  background-size: cover; ">
                <div class="row">
                    <h2><?php echo get_field('call_order_header') ?></h2>
                    <p><?php echo get_field('call_order_text') ?></p>
                    <?php echo do_shortcode('[contact-form-7 id="85" title="Contact form 1"]') ?>

                </div>

            </div>

        </div>

        <div id="programm-tur-section">
            <h2><?php echo get_field('title_the_tour_program') ?></h2>
            <div class="container">
                <div class="row">
                    <p><?php echo get_field('text_the_tour_program') ?></p>
                    <div id="arr_down_tour"></div>
                    <div><a href="#arr_down_tour"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/arr_down_w.png"/></a></div>
                </div>
            </div>
        </div>

        <?php $counter_day = 1; ?>
        <?php if (have_rows('Program_our')): ?>
            <?php while (have_rows('Program_our')): the_row(); ?>
                <?php
                $back_background = get_sub_field('back_background');
                ?>
                <div id="programm-tur-section-day-1" class="programm-tur-section-day"
                     style="background:
                             url(<?php echo $back_background['url']; ?>);
                             text-align: center;
                             background-size: cover;
                             padding-bottom: 55px;
                             position: relative;">
                    <h2><span><?php echo $counter_day; ?></span>день</h2>

                    <div class="container">
                        <div class="" id="slider_event">
                            <?php if (have_rows('day')): ?>
                                <?php while (have_rows('day')): the_row(); ?>

                                    <?php
                                    $image = get_sub_field('img');
                                    $time = get_sub_field('time');
                                    $title = get_sub_field('title');
                                    $description = get_sub_field('description');
                                    ?>
                                    <div class="cont_event col-md-6">
                                        <div class="event">
                                                <div class="img_content">
                                                    <div class="city b"><?php echo $time; ?></div>
                                                    <img src="<?php echo $image['url']; ?>" <?php echo $image['alt'] ?>/>
                                                </div>
                                                <p class="black"><?php echo $title; ?></p>
                                                <span><?php echo $description ?></span>
                                        </div>
                                    </div>

                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <?php $counter_day++; endwhile; ?>
        <?php endif; ?>

        <?php    get_template_part( 'template-parts/page/slider-swiper-gallery', 'page' ); ?>
        <?php
        get_template_part('template-parts/page/content', 'front-page');
        $imageBG2 = get_field('background_after_gallery')
        ?>
        <div id="ready-section" style="background: url(<?php echo $imageBG2['url']; ?>); background-size: cover;">

            <div class="container">
                <div class="row read">
                    <div class="col-md-5">
                        <h2>Решаешься?</h2>
                        <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ready_button.png"/></a>

                    </div>
                    <div class="col-md-7">
                        <p><?php echo get_field('text_resolved') ?></p>
                    </div>
                </div>
                <?php get_template_part('template-parts/navigation/navigation', 'top'); ?>
                <div id="dates-section">
                    <h2 class="elem" data-speed="5"><?php echo get_field('Title_coming_dates') ?></h2>
                    <div class="container flex">
                        <div class="row flex">
                            <div class="col-md-3 date_block">
                                <div class="swiper-button-prev1" href="#"><img
                                            src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow_up.png"/>Предыдущий
                                </div>

                                <div class="swiper-container swiper-container-tour-date" style="height: 130px">
                                    <div class="swiper-wrapper">
                                        <?php if (have_rows('dates')): ?>
                                            <?php while (have_rows('dates')): the_row(); ?>
                                                <?php
                                                $start_date =  get_sub_field('start_date');
                                                $end_date =  get_sub_field('end_date');
                                                $number_seats =  get_sub_field('number_seats');
                                                ?>
                                                <div class="swiper-slide" style="cursor: pointer">
                                                    <div class="date_tur"><span>Дата тура</span>
                                                        <h3 data-number-seats="<?php echo $number_seats ?>" class="black"><?php echo $start_date ?> - <?php echo $end_date?></h3></div>
                                                </div>
                                            <?php endwhile; ?>
                                        <?php endif; ?>
                                    </div>
                                    <!-- Add Pagination -->
                                    <div class="swiper-pagination"></div>
                                </div>


                                <div class="swiper-button-next1" href="#">Следующий<img
                                            src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow_down.png"/></div>
<!--                                <div class="calendar_icon"><img-->
<!--                                            src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/date-cal_icon.png"/></div>-->
                            </div>
                            <div class="col-md-9 event_bl">
                                <div class="event_info">
                                    <div class="free_m"><?php echo get_field('title_total_number') ?>
                                        <span></span></div>
                                    <h2 class="black"><?php the_title(); ?></h2>
                                    <p><?php
                                        $content = get_the_content();
                                        echo wp_trim_words($content, 18, '...'); ?></p>
                                    <div class="col-md-6 price black"><?php echo get_field('tour price') ?></div>
                                    <div class="col-md-6 book"><a data-toggle="modal"
                                                                  data-target="#exampleModalCenterReservation" href="#"
                                                                  class="b reservation">Забронировать место<img
                                                    src="<?php echo get_template_directory_uri(); ?>/assets/images/next_1.png"/></a></div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>



            </div>
        </div>

        <?php
        get_template_part('template-parts/page/content', 'front-page-panels');
        ?>


       <?php endwhile; // End of the loop.
        ?>

        </main><!-- #main -->
    </div><!-- #primary -->
    </div><!-- .wrap -->



<?php get_footer();
