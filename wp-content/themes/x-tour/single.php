<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package x-Tour
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
/*		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		*/?>

            <div id="single_post">
                <div class="page_heading">
                    <span class="post_date">
                        <?php echo get_the_date(); ?>
                    </span>
                    <span class="post_heading">
                        <?php echo the_title(); ?>
                    </span>
                    <div class="categories">

                    </div>
                </div>
                <div class="content">
                    <div class="container">
                        <div class="row">
                           <div class="col-md-8 col-md-offset-2">
                               <img class="post_thumbnail" src="<?php echo get_the_post_thumbnail_url() ?>" alt="Изображение записи">
                               <p class="post_content">
                                   <?php echo get_post_field('post_content', $post_id);?>
                               </p>
                               <p class="author">Автор: <span class="name"><?php echo the_author_meta( 'first_name' , $post->post_author ); ?> <?php echo the_author_meta( 'last_name' , $post->post_author ); ?></span></p>
                           </div>
                        </div>
                    </div>
                </div>
            </div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
