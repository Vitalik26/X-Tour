<?php
/**
 * x-Tour functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package x-Tour
 */

if (!function_exists('x_tour_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function x_tour_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on x-Tour, use a find and replace
         * to change 'x-tour' to the name of your theme in all the template files.
         */
        load_theme_textdomain('x-tour', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'menu-1' => esc_html__('Primary', 'x-tour'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('x_tour_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));
    }
endif;
add_action('after_setup_theme', 'x_tour_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function x_tour_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('x_tour_content_width', 640);
}

add_action('after_setup_theme', 'x_tour_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function x_tour_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'x-tour'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'x-tour'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'x_tour_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function x_tour_scripts()
{
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
    wp_enqueue_style('owl_carousel', get_template_directory_uri() . '/assets/css/owl.carousel.min.css');
    wp_enqueue_style('owl_default_theme', get_template_directory_uri() . '/assets/css/owl.theme.default.min.css');
    wp_enqueue_style('animate', get_template_directory_uri() . '/assets/css/animate.min.css');
    wp_enqueue_style('swiper', get_template_directory_uri() . '/assets/css/swiper.min.css');
    wp_enqueue_style('fancybox', get_template_directory_uri() . '/assets/css/jquery.fancybox.min.css');
    wp_enqueue_style('datatables', get_template_directory_uri() . '/assets/css/datatables.min.css');
    wp_enqueue_style('x-tour-style', get_stylesheet_uri());


    wp_enqueue_script('x-tour-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), '20151215', true);
    wp_enqueue_script('x-tour-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true);
    wp_enqueue_script('jQuery', get_theme_file_uri('/assets/js/jquery.min.js'));
    wp_enqueue_script('bootstrap', get_theme_file_uri('/assets/js/bootstrap.min.js'));
    wp_enqueue_script('html5', get_theme_file_uri('/assets/js/html5.js'), array(), '3.7.3');
    wp_enqueue_script('main', get_theme_file_uri('/assets/js/main.js'));
    wp_enqueue_script('wow', get_theme_file_uri('/assets/js/wow.min.js'));
    wp_enqueue_script('owl', get_theme_file_uri('/assets/js/owl.carousel.min.js'));
    wp_enqueue_script('swiper', get_theme_file_uri('/assets/js/swiper.min.js'));
    wp_enqueue_script('fancybox', get_theme_file_uri('/assets/js/jquery.fancybox.min.js'));
    wp_enqueue_script('datatables', get_theme_file_uri('/assets/js/jquery.dataTables.min.js'));
    wp_enqueue_script('true_loadmore', get_stylesheet_directory_uri() . '/assets/js/loadmore.js', array('jquery'));


    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'x_tour_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/jetpack.php';
}


//add option page start
if (function_exists('acf_add_options_page')) {
//    acf_add_options_sub_page();

    acf_add_options_page(
        array(
            'page_title' => 'options page',
            'menu_title' => 'Options page',
            'redirect' => 'false'
        )
    );

}//add option page end


// Receive the Request post that came from AJAX
add_action('wp_ajax_demo-pagination-load-posts', 'cvf_demo_pagination_load_posts');
// We allow non-logged in users to access our pagination
add_action('wp_ajax_nopriv_demo-pagination-load-posts', 'cvf_demo_pagination_load_posts');
function cvf_demo_pagination_load_posts()
{

    global $wpdb;
    // Set default variables
    $msg = '';

    if (isset($_POST['page'])) {
        // Sanitize the received page
        $page = sanitize_text_field($_POST['page']);
        $cur_page = $page;
        $page -= 1;
        // Set the number of results to display
        $per_page = 12;
        $previous_btn = true;
        $next_btn = true;
        $first_btn = true;
        $last_btn = true;
        $start = $page * $per_page;

        // Set the table where we will be querying data
        $table_name = $wpdb->prefix . "posts";

        // Query the necessary posts
        $all_blog_posts = $wpdb->get_results($wpdb->prepare("
            SELECT * FROM " . $table_name . " WHERE post_type = 'tours' AND post_status = 'publish' ORDER BY post_date DESC LIMIT %d, %d", $start, $per_page));

        // At the same time, count the number of queried posts
        $count = $wpdb->get_var($wpdb->prepare("
            SELECT COUNT(ID) FROM " . $table_name . " WHERE post_type = 'tours' AND post_status = 'publish'", array()));

        /**
         * Use WP_Query:
         *
         * $all_blog_posts = new WP_Query(
         * array(
         * 'post_type'         => 'post',
         * 'post_status '      => 'publish',
         * 'orderby'           => 'post_date',
         * 'order'             => 'DESC',
         * 'posts_per_page'    => $per_page,
         * 'offset'            => $start
         * )
         * );
         *
         * $count = new WP_Query(
         * array(
         * 'post_type'         => 'post',
         * 'post_status '      => 'publish',
         * 'posts_per_page'    => -1
         * )
         * );
         */

        // Loop into all the posts
        foreach ($all_blog_posts as $key => $post):
            $counter = 0;
            if (get_field('dates', $id)):
                while (has_sub_field('dates', $id)):
                    $start_date = get_sub_field('start_date', $id);
                    $end_date = get_sub_field('end_date', $id);
                    if (get_row_index() == 4) break;
                    if ($counter >= 1) {
                        $msg .= '<span class="devider">|</span>';
                    }
                    ?>
                    <a href=" get_permalink($id); ?>"><?php echo $start_date; ?>
                        - <?php echo $end_date; ?></a>
                    <?php
                    ?>
                    <?php $counter++; endwhile; ?>
            <?php endif;


            $msg .= '
            <div class="cont_event col-md-6">
                <div class="event"><a href="' . get_permalink($post->ID) . '">
                        <div class="img_content">
                            <div class="city">' . get_field('address_event', $post->ID) . '</div>
                            <img src="' . get_the_post_thumbnail_url($post->ID) . '" alt="">
                        </div>
                        <p class="black">' . $post->post_title . '</p></a>
                      
                            <span>';
            $counter = 0;
            if (get_field('dates', $post->ID)):
                while (has_sub_field('dates', $post->ID)):
                    $start_date = get_sub_field('start_date', $post->ID);
                    $end_date = get_sub_field('end_date', $post->ID);
                    if (get_row_index() == 4) break;
                    if ($counter >= 1) {
                        $msg .= '<span class="devider">|</span>';
                    };
                    $msg .= ' <a href="' . get_permalink($post->ID) . '">' . $start_date . '
                                                  - ' . $end_date . '</a>';
                    $counter++; endwhile;
            endif;
            $msg .= '</span>
                               
                          </div>
            </div>';

        endforeach;


        // Optional, wrap the output into a container
//        $msg = "<div class='cvf-universal-content'>" . $msg . "</div><br class = 'clear' />";

        // This is where the magic happens
        $no_of_paginations = ceil($count / $per_page);

        if ($cur_page >= 7) {
            $start_loop = $cur_page - 3;
            if ($no_of_paginations > $cur_page + 3)
                $end_loop = $cur_page + 3;
            else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                $start_loop = $no_of_paginations - 6;
                $end_loop = $no_of_paginations;
            } else {
                $end_loop = $no_of_paginations;
            }
        } else {
            $start_loop = 1;
            if ($no_of_paginations > 7)
                $end_loop = 7;
            else
                $end_loop = $no_of_paginations;
        }

        // Pagination Buttons logic
        $pag_container .= "
        <div class='cvf-universal-pagination'>
            <div class='cvf-universal-pagination-pagin'>";

//        if ($first_btn && $cur_page > 1) {
//            $pag_container .= "<li p='1' class='active'>First</li>";
//        } else if ($first_btn) {
//            $pag_container .= "<li p='1' class='inactive'>First</li>";
//        }
//
//        if ($previous_btn && $cur_page > 1) {
//            $pre = $cur_page - 1;
//            $pag_container .= "<li p='$pre' class='active'>Previous</li>";
//        } else if ($previous_btn) {
//            $pag_container .= "<li class='inactive'>Previous</li>";
//        }
        for ($i = $start_loop; $i <= $end_loop; $i++) {

            if ($cur_page == $i)
                $pag_container .= "<span p='$i' class = 'selected pagin' >{$i}</span>";
            else
                $pag_container .= "<span class = 'wrap_pagin'><span p='$i' class='active pagin'>{$i}</span></span>";
        }

//        if ($next_btn && $cur_page < $no_of_paginations) {
//            $nex = $cur_page + 1;
//            $pag_container .= "<li p='$nex' class='active'>Next</li>";
//        } else if ($next_btn) {
//            $pag_container .= "<li class='inactive'>Next</li>";
//        }
//
//        if ($last_btn && $cur_page < $no_of_paginations) {
//            $pag_container .= "<li p='$no_of_paginations' class='active'>Last</li>";
//        } else if ($last_btn) {
//            $pag_container .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
//        }

        $pag_container = $pag_container . "
            </div>
        </div>";
        // We echo the final output
        echo
            $msg .
            '<div class = "cvf-pagination-nav">' . $pag_container . '</div>';

    }
    // Always exit to avoid further execution
    exit();
}


function true_load_posts()
{
    $args = unserialize(stripslashes($_POST['query']));
    $args['paged'] = $_POST['page'] + 1; // следующая страница
    $args['post_status'] = 'publish';
    // обычно лучше использовать WP_Query, но не здесь
    query_posts($args);
    // если посты есть
    if (have_posts()) :
        // запускаем цикл
        while (have_posts()): the_post();
            ?>
            <div class="cont_event col-md-6 ">
                <div class="event">
                    <a href="<?php echo get_permalink(); ?>">
                        <div class="img_content">
                            <div class="city"><?php echo get_field('address_event'); ?></div>
                            <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="">
                        </div>
                        <p class="black"><?php the_title(); ?></p>
                        <span>
                              <?php
                              $counter = 0;
                              if (get_field('dates')): ?>
                                  <?php while (has_sub_field('dates')): ?>
                                      <?php
                                      if ( $counter < 3 ) {
                                          $start_date = get_sub_field('start_date');
                                          $end_date = get_sub_field('end_date');

                                          if ($counter >= 1) {
                                              ?><span class="devider">|</span><?php
                                          }
                                          ?>
                                          <?php echo $start_date; ?>
                                          - <?php echo $end_date; ?>
                                          <?php
                                          ?>
                                          <?php $counter++;
                                      }
                                  endwhile;
                              endif; ?>
                        </span>
                     </div>
                </a>
            </div>

        <?php endwhile;

    endif;
    die();
}


add_action('wp_ajax_loadmore', 'true_load_posts');
add_action('wp_ajax_nopriv_loadmore', 'true_load_posts');