
<div id="slider-photos">

    <div class="swiper-container swiper-container-gallery ">
        <div class="swiper-wrapper">

            <?php if (have_rows('images_slider')): ?>
                <?php while (have_rows('images_slider')): the_row();

                    $img = get_sub_field('images');?>
                    <div  class="swiper-slide"><a data-fancybox="gallery" href="<?php echo $img['url']; ?>"><img  class="img_galery" src="<?php echo $img['url']; ?>"/></a></div>

                <?php endwhile; ?>
            <?php endif; ?>

        </div>

        <img class="swiper-button-prevG" src="<?php echo get_template_directory_uri(); ?>/assets/images/prev_2.png" />
        <img class="swiper-button-nextG" src="<?php echo get_template_directory_uri(); ?>/assets/images/next_2.png" />
    </div>

</div>