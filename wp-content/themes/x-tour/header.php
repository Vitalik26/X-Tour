<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
 <?php wp_head(); ?>

<script>
  jQuery(function($){
   $(".telephone").mask("(999) 999-9999");
   });
new WOW().init();
</script>
</head>
<body>
    <div id="menu-header" class="navbar-fixed-top">
            <div class="container-fluid">
                <div class="row flex" style="flex-wrap: wrap;">
                    <div class="col-md-6 col-sm-7 left_menu">
                        <a href="<?php echo get_home_url(); ?>" class="logo">

                            <?php
                            $image = get_field('logo_img' , 'option');
                            if( !empty($image) ): ?>
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                            <?php endif; ?>

                        </a>
                        <div class="phone">
                        <?php if(get_field('phones' , 'option')): ?>
                            <?php while(has_sub_field('phones' , 'option')): ?>
                                <a href="tel:<?php echo get_sub_field('phone_part1red' , 'option') ?> <?php echo get_sub_field('phone_part2white' , 'option') ?>""><span style="color: #ed3f1c"><?php echo get_sub_field('phone_part1red' , 'option') ?></span><?php echo get_sub_field('phone_part2white' , 'option') ?></a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        </div>
                        <div class="social">

                            <?php if(get_field('social' , 'option')): ?>
                                    <?php while(has_sub_field('social' , 'option')): ?>
                                <?php $image = get_sub_field('image', 'option');
                                    $link = get_sub_field('link' , 'option');
                                    ?>
                                    <a href="<?php echo $link; ?>" class=""><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" /></a>
                                    <?php endwhile; ?>
                            <?php endif; ?>

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-5 text-right">
                      <div class="row">
                        <ul class="top-right-menu">
                            <?php wp_nav_menu( array( 'default' => 'footer_header_menu' ) ); ?>

                        </ul>
                          <ul style="padding-left: 10px;" class="top-right-menu ">
<!--                            <li><a href="#">EN</a></li><b style="color: #fff;    font-size: 11px;">/</b>-->
<!--                            <li><a href="#">RU</a></li>-->
                              <li><a href="#">  </a></li><b style="color: #fff;    font-size: 11px;"></b>
                              <li><a href="#">  </a></li>

                        </ul>
                      </div>
                     <div class="row top-right-bottom hidden-xs">
                         <?php if(get_field('custom_menu_under_the_main_menu' , 'option')): ?>
                             <?php while(has_sub_field('custom_menu_under_the_main_menu' , 'option')): ?>
                                 <?php $image = get_sub_field('img', 'option');
                                 $title_page = get_sub_field('title_page' , 'option');
                                 $link = get_sub_field('link_to_page' , 'option');
                                 ?>
                                 <div style="margin-right: 25px;"><a href="<?php echo $link; ?>"><img style="height: 17px;"  src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>"/><?php echo  $title_page; ?></a></div>
                             <?php endwhile; ?>
                         <?php endif; ?>
                     </div>


                    </div>

                    <div class="col-md-2 col-sm-12 hidden-xs afterp" >
                         <a  href="<?php echo get_permalink( get_page_by_path( 'afterparty' ) ) ?>" class="top-right-button">AFTERPARTY</a>

                    </div>


                </div>

             </div>


        <!-- Modal Забронировать место -->
        <div class="modal fade" id="exampleModalCenterReservation" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-reservation" role="document">
                <div class="modal-content modal-content-reservation">
                    <div class="modal-body modal-body-reservation">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <button type="button" style="background: transparent" class="btn btn-secondary" data-dismiss="modal"></button>
                        <div  id="call-section"  style=" background: url(<?php echo get_field('background_before_tour_program_down')['url'] ?>);  background-size: cover;">
                            <div class="row" >
                                <h2>ЗАБРОНИРОВАТЬ МЕСТО</h2>
                                <?php echo do_shortcode('[contact-form-7 id="105" title="Contact form Reservation"]') ?>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

    <div id="main">