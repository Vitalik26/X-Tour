<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package x-Tour
 */

get_header();
?>
    <div class="programm_b_h" >
        <h2>Новости</h2>
    </div>

    <div id="press-section" class="blog_news">
        <div class="container">
            <div class="row row_blog_news">
                <?php
                if ( have_posts() ) :
                    while ( have_posts() ) : the_post();?>
                        <div class="col-md-6 cont_press">
                            <a href="<?php echo get_permalink(); ?>" class="img_content">
                                <div class="img_d"><img class="img_post"
                                                        src="<?php echo get_the_post_thumbnail_url() ?>"/></div>
                                <h3 class="black"><?php echo the_title(); ?></h3>
                                <p><?php echo wp_trim_words(get_the_content(), 12); ?></p>

                            </a>
                        </div>

               <?php     endwhile;
                endif;
                the_posts_pagination( array(
                    'prev_text' => __( '', 'textdomain' ),
                    'next_text' => __( '', 'textdomain' ),
                ) );

                get_next_posts_link()
                ?>

            </div>

        </div>
    </div>

<?php
get_footer();
